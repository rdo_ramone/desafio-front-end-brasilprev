# ListaUsuarios

Projeto gerado via Angular CLI.

## Instalação

Após baixar o projeto, no diretório do mesmo, execute o comando `npm i` para instalar as dependências.

## Server de Desenvolvimento

Para rodar o projeto, execute o comando `ng serve`. Acesse a url `http://localhost:4200/` para poder visualizar o projeto.
