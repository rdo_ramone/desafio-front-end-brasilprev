import { StreetModel } from './street.model';

export class LocationModel {
  city: string;
  country: string;
  postcode: number;
  state: string;
  street: StreetModel;
}
