import { DobModel } from './dob.model';
import { LocationModel } from './location.model';
import { NameModel } from './name.model';
import { PictureModel } from './picture.model';

export class UserModel {
  id: string;
  cell: string;
  dob: DobModel;
  email: string;
  gender: string;
  location: LocationModel;
  name: NameModel;
  nat: string;
  phone: string;
  picture: PictureModel;
  length: number;
}
