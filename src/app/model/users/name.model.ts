export class NameModel {
  first: string;
  last: string;
  title: string;
}
