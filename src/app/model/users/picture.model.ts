export class PictureModel {
  large: string;
  medium: string;
  thumbnail: string;
}
