import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { UserDetailsComponent } from './components/user/user-details/user-details.component';

const routes: Routes = [
  {
    path: 'usuarios',
    component: UserComponent
  },
  {
    path: 'detalhes-usuario/:id',
    component: UserDetailsComponent
  },
  {
    path: '',
    redirectTo: '/usuarios',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
