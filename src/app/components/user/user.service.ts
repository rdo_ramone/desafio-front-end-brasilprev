import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { UserModel } from '../../model/users/user.model';
import { ResultsModel } from '../../model/users/results.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly url = 'https://randomuser.me/api/';
  private users: UserModel;
  private user: UserModel;

  constructor(private http: HttpClient) { }

  getUsers() {
    const params = new HttpParams()
      .set('results', '50')
      .set('nat', 'br');

    return this.http.get(this.url, { params })
      .pipe(
        tap((res: ResultsModel) => {
          return this.users = this.setId(res.results);
        })
      );
  }

  getUser(id: string): UserModel {
    const newData = Object.assign({}, this.users);
    const keys = Object.keys(newData);

    keys.forEach(key => {
      if (newData[key].id === id) {
        this.user = newData[key];
      }
    });

    return this.user;
  }

  private setId(data: UserModel): UserModel {
    const newData = Object.assign({}, data);
    const keys = Object.keys(newData);

    keys.forEach((key, i) => {
      newData[key].id = (i + 1).toString();
    });

    return newData;
  }
}
