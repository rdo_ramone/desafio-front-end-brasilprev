import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { UserModel } from '../../model/users/user.model';
import { ResultsModel } from '../../model/users/results.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public users: UserModel;

  constructor(private userservice: UserService) { }

  ngOnInit() {
    this.listUsers();
  }

  listUsers() {
    this.userservice.getUsers()
      .subscribe((data: ResultsModel) => {
        this.users = data.results;
      });
  }

  getUsers(): UserModel {
    return this.users;
  }
}
