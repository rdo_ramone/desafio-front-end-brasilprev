import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderByName'
})
export class OrderByNamePipe implements PipeTransform {
  transform(array: any, field: string): any[] {
    if (!Array.isArray(array)) {
      return;
    }
    array.sort((a: any, b: any) => {
      if (a.name[field] < b.name[field]) {
        return -1;
      } else if (a.name[field] > b.name[field]) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
